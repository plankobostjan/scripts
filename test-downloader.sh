#!/bin/bash

extract_checksum(){
    printf $(curl $1 | grep $2 | awk '{print $1; exit}')
}

download_stuff(){
    URLs=(https://download.configserver.com/csf.tgz \
        https://gitlab.com/plankobostjan/scripts/-/raw/master/config-samples/sshd_config_sample \
        https://gitlab.com/plankobostjan/scripts/-/raw/master/config-samples/csf.conf.sample)

    #CSF_INSTALL_CHECKSUM=$(curl https://www.configserver.com/checksums.txt | grep csf.tgz | awk '{print $1; exit}')
    #CSF_CONFIG_CHECKSUM=$(curl https://gitlab.com/plankobostjan/scripts/-/raw/master/checksums.txt | grep csf.conf.sample | awk '{print $1; exit}')
    #SSH_CONFIG_CHECKSUM=$(curl https://gitlab.com/plankobostjan/scripts/-/raw/master/checksums.txt | grep sshd_config_sample | awk '{print $1; exit}')

    CSF_INSTALL_CHECKSUM=$(extract_checksum https://www.configserver.com/checksums.txt csf.tgz)
    CSF_CONFIG_CHECKSUM=$(extract_checksum https://gitlab.com/plankobostjan/scripts/-/raw/master/checksums.txt csf.conf.sample)
    SSH_CONFIG_CHECKSUM=$(extract_checksum https://gitlab.com/plankobostjan/scripts/-/raw/master/checksums.txt sshd_config_sample)

    for url in ${URLs[*]}
    do
        wget -O /tmp/$(echo "$url" | rev | cut -d / -f 1 | rev) $url
    done
}
download_stuff
echo $CSF_INSTALL_CHECKSUM
echo $CSF_CONFIG_CHECKSUM
echo $SSH_CONFIG_CHECKSUM
