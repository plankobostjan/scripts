backup(){
    echo "Creating backup of file $1 ..."
    new_name="$1.$(date +%Y-%m-%d.%H%M.bak)"
    cp -p $1 $new_name
    echo "Backed up $1 to $new_name!"
}

fbackup(){
    echo "Creating backup of folder $1 ..."
    new_name="$1.$(date +%Y-%m-%d.%H%M.bak)"
    cp -pr $1 $new_name
    echo "Backed up $1 to $new_name!"
}

isempty(){
    if ! grep -q '[^[:space:]]' $1
    then
        echo "$1 is empty!"
    else
        echo "$1 is not empty!"
    fi
}
