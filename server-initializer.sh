#!/bin/bash

extract_checksum(){
    printf $(curl $1 | grep $2 | awk '{print $1; exit}')
}

download_stuff(){
    URLs=(https://download.configserver.com/csf.tgz \
        https://gitlab.com/plankobostjan/scripts/-/raw/master/config-samples/sshd_config_sample \
        https://gitlab.com/plankobostjan/scripts/-/raw/master/config-samples/csf.conf.sample)

    CSF_INSTALL_CHECKSUM=$(extract_checksum https://www.configserver.com/checksums.txt csf.tgz)
    CSF_CONFIG_CHECKSUM=$(extract_checksum https://gitlab.com/plankobostjan/scripts/-/raw/master/checksums.txt csf.conf.sample)
    SSH_CONFIG_CHECKSUM=$(extract_checksum https://gitlab.com/plankobostjan/scripts/-/raw/master/checksums.txt sshd_config_sample)

    for url in ${URLs[*]}
    do
        wget -O /tmp/$(echo "$url" | rev | cut -d / -f 1 | rev) $url
    done
}

check_integrity(){
    CSF_INSTALL_SHA_SUM_MATCH=0
    SSH_CONFIG_SHA_SUM_MATCH=0
    CSF_CONFIG_SHA_SUM_MATCH=0
    if [ $(sha256sum /tmp/csf.tgz | awk '{print $1; exit}') == $CSF_INSTALL_CHECKSUM ]
    then
        CSF_INSTALL_SHA_SUM_MATCH=1
    fi
    if [ $(sha256sum /tmp/csf.conf.sample | awk '{print $1; exit}') == $CSF_CONFIG_CHECKSUM ]
    then
        CSF_CONFIG_SHA_SUM_MATCH=1
    fi
    if [ $(sha256sum /tmp/sshd_config_sample | awk '{print $1; exit}') == $SSH_CONFIG_CHECKSUM ]
    then
        SSH_CONFIG_SHA_SUM_MATCH=1
    fi
}

install_csf(){
    tar -xzf /tmp/csf.tgz -C /tmp
    cd /tmp/csf/
    sh install.sh
}

deploy_csf_config(){
    cp /etc/csf/csf.conf /etc/csf/csf.conf.orig
    cp /tmp/csf.conf.sample /etc/csf/csf.conf
}

deploy_ssh_config(){
    if [ -f /etc/sysconfig/sshd ]
    then
        # Disable system-wide crypto policy
        echo "CRYPTO_POLICY=" >> /etc/sysconfig/sshd
    fi

    cp /etc/ssh/sshd_config /etc/ssh/sshd_config_orig
    cp /tmp/sshd_config_sample /etc/ssh/sshd_config
}

harden_ssh(){
    # Source: https://www.sshaudit.com/hardening_guides.html#ubuntu_20_04_lts

    # Re-generate the RSA and ED25519 keys
    rm /etc/ssh/ssh_host_*
    ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ""
    ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ""

    # Remove small Diffie-Hellman moduli
    awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.safe
    mv /etc/ssh/moduli.safe /etc/ssh/moduli
}

add_new_user(){
    read -p "Please enter the username for the new user: " NEW_USER
    while true; do
        read -p "Your new user is $NEW_USER. OK? [Y/N]: " yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) printf "Oh, well..\nGoodbye!\n"; exit 1; break;;
                * ) echo "Please answer yes or no.";;
        esac
    done
    useradd -m -G sudo $NEW_USER
    passwd $NEW_USER
}

sanity_check(){
    echo "Checking integrity of downloaded files.."
    if [ "$((CSF_INSTALL_SHA_SUM_MATCH))" == 1 ]
    then
        echo "Checksum of CSF installation file is OK."
    else
        echo "Checksum of CSF installation file is NOT OK."
    fi
    if [ "$((CSF_CONFIG_SHA_SUM_MATCH))" == 1 ]
    then
        echo "Checksum of CSF config file is OK."
    else
        echo "Checksum of CSF config file is NOT OK."
    fi
    if [ "$((SSH_CONFIG_SHA_SUM_MATCH))" == 1 ]
    then
        echo "Checksum of SSH config file is OK."
    else
        echo "Checksum of SSH config file is NOT OK."
    fi
    if [ "$((SSH_CONFIG_SHA_SUM_MATCH))" == 1 ] && [ "$((CSF_CONFIG_SHA_SUM_MATCH))" == 1 ] && [ "$((CSF_INSTALL_SHA_SUM_MATCH))" == 1 ]
    then
        echo "---------------------------------------------------"
        echo "All checksums match!"
        read -p "Shall I proceed? [Y/N]: " -n 1 -r
        echo    # (optional) move to a new line
        if [[ ! $REPLY =~ ^[Yy]$ ]]
        then
            printf "Oh, well..\nGoodbye!\n"
            exit 1
        fi
        echo "Great! Moving on!"
        echo "---------------------------------------------------"
    else
        echo "---------------------------------------------------"
        echo "Some checksums don't match! Check the sources before proceeding."
        echo "Exiting..."
        exit 1
    fi
}

restart_services(){
    echo "Restarting CSF..."
    csf -ra > /dev/null
    echo "CSF restarted"
    echo "Restarting SSH..."
    systemctl restart sshd > /dev/null
    echo "SSH restarted"
}

download_stuff
check_integrity
sanity_check
add_new_user
install_csf
deploy_csf_config
deploy_ssh_config
harden_ssh
restart_services
echo "All done!"
echo "Bye."
